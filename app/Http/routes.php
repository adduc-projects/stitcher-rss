<?php declare(strict_types=1);

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', 'Controller@landing');
$router->get('/login', 'User@login');
$router->post('/login', 'User@login');
$router->get('/logout', 'User@logout');

$router->group(['middleware' => 'auth:session'], function () use ($router) {
    $router->addRoute(['GET', 'POST'], '/search', 'Controller@search');
});

$router->group(['middleware' => 'auth:basic'], function () use ($router) {
    $router->get('/shows/{show_id}/feed', 'Controller@showFeed');
});

$router->group(['middleware' => 'auth:route'], function () use ($router) {
    $router->get(
        '/shows/{show_id}/episodes/{rss_user}/{rss_pass}/{episode_id}.mp3',
        'Controller@showEpisode'
    );
});

$router->get('/metrics', 'Metric@index');
