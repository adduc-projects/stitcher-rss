<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Metric extends BaseController
{
    public function index(Request $request)
    {
        $is_user = $request->getUser() === env('METRIC_USER', '');
        $is_pass = $request->getPassword() === env('METRIC_PASS', '');

        if (!$is_user || !$is_pass) {
            $headers = ['WWW-Authenticate' => 'Basic realm="Metrics"'];
            return response("Unauthorized\n", 401, $headers);
        }

        $table = app('db')->table('users');
        $all_count = $table->count();
        $active_count = $table->whereRaw('expiration > NOW()')->count();

        $host_labels = [
            'site' => 'stitcher_rss'
        ];

        $metrics = [
            [
                'name' => 'users',
                'labels' => ['type' => 'all'],
                'value' => $all_count,
            ],
            [
                'name' => 'users',
                'labels' => ['type' => 'active'],
                'value' => $active_count,
            ],
        ];

        $view = view('metrics', [
            'host_labels' => $host_labels,
            'metrics' => $metrics
        ]);

        return response($view, 200, [
            'Content-Type' => 'text/plain; version=0.0.4'
        ]);
    }
}
