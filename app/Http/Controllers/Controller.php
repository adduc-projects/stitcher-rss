<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Adduc\Stitcher\Api;
use Adduc\Stitcher\Client;
use DateTime;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    public function landing()
    {
        session_start();
        return view('landing');
    }

    public function search(Request $request, Client $client)
    {
        $term = $request->input('term');

        if (!$term) {
            return view('search-form');
        }

        $response = $client->Search([
            'uid' => $this->getPremiumUserId($request),
            'term' => $term,
        ]);

        $view = view('search-results', [
            'response' => $response,
        ])->render();

        return response($view, 200);
    }

    /**
     * Renders RSS for a show.
     */
    public function showFeed(Request $request, Client $client, string $show_id)
    {
        $filestore = new \Illuminate\Cache\FileStore(
            new \Illuminate\Filesystem\Filesystem(),
            storage_path('cache/show-feed')
        );

        $cache = new \Illuminate\Cache\Repository($filestore);

        $feed = env('CACHE_FEED') ? $cache->get($show_id) : false;

        if (!$feed) {
            $feed = $this->getShowFeed($request, $client, $show_id);
            $cache->put($show_id, $feed, 29);
        }

        if ($feed->maintenance_error) {
            $view = [
                "Stitcher is undergoing maintenance.",
                503,
                []
            ];
            return response($view[0], $view[1], $view[2]);
        }

        $view_data = compact('request', 'feed', 'client');
        $view = [
            view('rss-feed', $view_data)->render(),
            200,
            ['Content-Type' => 'application/rss+xml; charset=utf-8']
        ];

        return response($view[0], $view[1], $view[2]);
    }

    /**
     * Fetches show information and all episodes of every season
     */
    protected function getShowFeed(
        Request $request,
        Client $client,
        string $show_id
    ): Api\GetFeedDetailsWithEpisodes\Response {
        $parameters = [
            'fid' => $show_id,
            's' => 0,
            'uid' => $this->getPremiumUserId($request),
        ];

        $feed = $client->GetFeedDetailsWithEpisodes($parameters);
        $feed->episodes->episodes = [];

        $fetch = function ($parameters) use (&$feed, $client) {
            $feed_tmp = $client->GetFeedDetailsWithEpisodes($parameters);

            $feed_tmp->episodes->episodes = array_merge(
                $feed_tmp->episodes->episodes,
                $feed->episodes->episodes
            );
            $feed = $feed_tmp;
        };

        if (empty($feed->feed->seasons)) {
            $parameters['max_epi'] = 50;
            do {
                $fetch($parameters);
                $parameters['s'] += $parameters['max_epi'];
            } while ($feed->feed->episodeCount > count($feed->episodes->episodes));
        } else {
            foreach ($feed->feed->seasons as $season) {
                $parameters['id_Season'] = $season->id;
                $fetch($parameters);
            }
        }

        $episodes = [];
        // Deduplicate off of ID
        // If a new episode is released as we refresh, we could get it
        // in the feed twice through pagination.
        foreach ($feed->episodes->episodes as $episode) {
            $episodes[$episode->id] = $episode;
        }

        $feed->episodes->episodes = array_values($episodes);

        return $feed;
    }

    protected function getPremiumUserId(Request $request): int
    {
        if ($request->user()->expiration > new DateTime()) {
            return (int)$request->user()->stitcher_id;
        }

        /** @todo move out of controller. */
        /** @todo cache for more efficient show feed / feed list cache utilization */
        $sql = 'SELECT stitcher_id FROM users WHERE expiration > NOW() ORDER BY expiration DESC LIMIT 1';
        $users = app('db')->select($sql);
        return $users[0]->stitcher_id;
    }

    /**
     * Redirects to the episode's enclosure URL. Used to workaround a
     * few podcast clients that cache episode URLs, causing issues when
     * the user attempts to download older episodes at a later date.
     */
    public function showEpisode(
        Request $request,
        Client $client,
        string $show_id,
        string $rss_user,
        string $rss_pass,
        string $episode_id
    ) {
        $filestore = new \Illuminate\Cache\FileStore(
            new \Illuminate\Filesystem\Filesystem(),
            storage_path('cache/episode-enclosure')
        );

        $cache = new \Illuminate\Cache\Repository($filestore);

        $view = $cache->get($episode_id);
        if (env('CACHE_FEED') && $view) {
            return response($view[0], $view[1], $view[2]);
        }

        $feed = $this->getShowFeed($request, $client, $show_id);

        if ($feed->maintenance_error) {
            $view = [
                "Stitcher is undergoing maintenance.",
                503,
                []
            ];
            $cache->put($episode_id, $view, 2);

            return response($view[0], $view[1], $view[2]);
        }

        $show_episode = [];

        foreach ($feed->episodes->episodes as $episode) {
            if ($episode->id == $episode_id) {
                $show_episode = $episode;
                break;
            }
        }

        if (!$show_episode) {
            $view = [
                "Unknown show / episode.",
                404,
                []
            ];
            $cache->put($episode_id, $view, 10);

            return response($view[0], $view[1], $view[2]);
        }

        $url = $show_episode->url;

        $view = [
            '',
            302,
            ['Location' => $url]
        ];

        $cache->put($episode_id, $view, 10);

        return response($view[0], $view[1], $view[2]);
    }
}
