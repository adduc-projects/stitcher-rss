@extends('layout')

@section('content')
<div class="columns is-desktop">
    <div class="column is-half is-offset-one-quarter">
        @if (empty($result->subscriptionExpiration))
            <h1 class="title is-2">Uh-oh! You aren't a subscriber.</h1>
            <p>
                Stitcher has no record of your account ever having a premium
                subscription. Did you sign up using a different username?
            </p>
        @else
            <h1 class="title is-2">Uh-oh! Your subscription has expired.</h1>
            <p>
                Stitcher reports your subscription expired on
                {{ $result->subscriptionExpiration }}.
            </p>
        @endif
    </div>
</div>

<div class="columns is-desktop">
    <div class="column is-half is-offset-one-quarter">
        <a href="/login" class="button is-primary">Back to Login</a>
    </div>
</div>

<div class="columns is-desktop">
    <div class="column is-half is-offset-one-quarter">
        <h2 class="title is-3">Additional Info:</h2>
        <ul>
            <li>Email: {{ $result->email }}</li>
            <li>Stitcher ID: {{ $result->id }}</li>
        </ul>
    </div>
</div>
@endsection
